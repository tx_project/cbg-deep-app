/**
 * Created by yangdeng on 2016/12/11.
 */
'use strict';
(function () {

    var mainService = function ($http) {
        var _self = this;

        _self.getData = function (data) {
            var list = [];
            angular.forEach(data, function (v, k) {
                if (v) {
                    list.push(k + '=' + v);
                }
            });
            var str = list.join('&');
            return $http.post('/api/query', str,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}});
        };

        // return {
        //     'getData': _self.getData
        // };
    };

    mainService.$inject = ['$http'];

    angular.module('main.service', [])
        .service('mainService', mainService);

})();