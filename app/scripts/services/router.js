/**
 * Created by yangdeng on 2016/12/10.
 */
'use strict';
(function () {

    var router = function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('welcome', {
                url: '/welcome',
                templateUrl: 'partials/welcome.html',
                controller: 'welCtr'
            })
            .state('home', {
                url: '/home',
                templateUrl: 'partials/home.html',
                controller: 'homeCtr'
            });

        $urlRouterProvider.otherwise('/welcome');
    };

    router.$inject = ['$stateProvider', '$urlRouterProvider'];

    angular.module('router.config', [])
        .config(router);

})();