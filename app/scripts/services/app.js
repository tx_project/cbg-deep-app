/**
 * Created by yangdeng on 2016/12/10.
 */
'use strict';
(function () {

    angular.module('myApp.services',[
        'router.config',
        'main.service'
    ]);

})();