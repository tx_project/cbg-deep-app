/**
 * Created by yangdeng on 2016/12/10.
 */
'use strict';
(function () {

    var homeCtr = function ($rootScope, $scope, mainService) {

        $rootScope.hasLink = false;

        $scope.valueData = {
            pageNo: 1,
            pageSize: 10
        };

        var homeData = $scope.homeData = {
            schoolData: [
                {
                    name: '全部',
                    value: 'all',
                    checked: true
                },
                {
                    name: '荒火教',
                    value: '1'
                },
                {
                    name: '天机营',
                    value: '2'
                },
                {
                    name: '翎羽山庄',
                    value: '3'
                },
                {
                    name: '魍魉',
                    value: '4'
                },
                {
                    name: '太虚观',
                    value: '5'
                },
                {
                    name: '云麓仙居',
                    value: '6'
                },
                {
                    name: '冰心堂',
                    value: '7'
                },
                {
                    name: '弈剑听雨阁',
                    value: '8'
                },
                {
                    name: '鬼墨',
                    value: '9'
                },
                {
                    name: '龙巫',
                    value: '10'
                },
                {
                    name: '幽篁',
                    value: '11'
                }
            ],
            schoolMap: {
                '1': '荒火教',
                '2': '天机营',
                '3': '翎羽山庄',
                '4': '魍魉',
                '5': '太虚观',
                '6': '云麓仙居',
                '7': '冰心堂',
                '8': '弈剑听雨阁',
                '9': '鬼墨',
                '10': '龙巫',
                '11': '幽篁'
            }
        };

        var fn = $scope.fn = {
            chooseSchool: function (e, item) {
                e.stopPropagation();
                if (item.value != 'all') {
                    item.checked = true;
                    angular.forEach(homeData.schoolData, function (i) {
                        if (i.value == 'all') {
                            i.checked = false;
                        }
                    });
                }
                else {
                    item.checked = true;
                    angular.forEach(homeData.schoolData, function (i) {
                        if (i.value != 'all') {
                            i.checked = false;
                        }
                    });
                }
            },
            changeShowBase: function (e) {
                e.stopPropagation();
                homeData.isShowBase = !homeData.isShowBase;
            },
            changeShowSenior: function (e) {
                e.stopPropagation();
                homeData.isShowSenior = !homeData.isShowSenior;
            },
            changeShowTouch: function (e) {
                e.stopPropagation();
                homeData.isShowTouch = !homeData.isShowTouch;
            },
            changeShowDefense: function (e) {
                e.stopPropagation();
                homeData.isShowDefense = !homeData.isShowDefense;
            },
            changeShowIncase: function (e) {
                e.stopPropagation();
                homeData.isShowIncase = !homeData.isShowIncase;
            },
            changeShowChildren: function (e) {
                e.stopPropagation();
                homeData.isShowChildren = !homeData.isShowChildren;
            },
            changeShowAwaken: function (e) {
                e.stopPropagation();
                homeData.isShowAwaken = !homeData.isShowAwaken;
            },
            changeShowTrick: function (e) {
                e.stopPropagation();
                homeData.isShowTrick = !homeData.isShowTrick;
            },
            changeShowEle: function (e) {
                e.stopPropagation();
                homeData.isShowEle = !homeData.isShowEle;
            },
            changeShowDress: function (e) {
                e.stopPropagation();
                homeData.isShowDress = !homeData.isShowDress;
            },
            changeShowOther: function (e) {
                e.stopPropagation();
                homeData.isShowOther = !homeData.isShowOther;
            },
            getFilterData: function () {
                var putData = angular.copy($scope.valueData);
                var list = [];
                angular.forEach(homeData.schoolData, function (i) {
                    if (i.value == 'all' && i.checked) {
                        // putData['school'] = '';
                    }
                    else {
                        if (i.checked) {
                            list.push(i.value);
                        }
                    }
                });
                if (!putData['school'] && list.length > 0) {
                    putData['school'] = list.join(',');
                }
                putData['haiziTianyu'] = (putData['haiziTianyu']) ? (1) : (0);
                putData['huikanfanghu'] = (putData['huikanfanghu']) ? (1) : (0);
                putData['duncifanghu'] = (putData['duncifanghu']) ? (1) : (0);
                putData['huoyuanfanghu'] = (putData['huoyuanfanghu']) ? (1) : (0);
                putData['shuifengdufanghu'] = (putData['shuifengdufanghu']) ? (1) : (0);
                putData['wanfeng'] = (putData['wanfeng']) ? (1) : (0);
                putData['huxin'] = (putData['huxin']) ? (1) : (0);
                putData['ligntMenpai'] = (putData['ligntMenpai']) ? (1) : (0);
                putData['qinghua'] = (putData['qinghua']) ? (1) : (0);
                putData['xuansu'] = (putData['xuansu']) ? (1) : (0);
                putData['guhong'] = (putData['guhong']) ? (1) : (0);
                putData['xiangyun'] = (putData['xiangyun']) ? (1) : (0);
                putData['tinglan'] = (putData['tinglan']) ? (1) : (0);
                putData['haitang'] = (putData['haitang']) ? (1) : (0);
                putData['feihuhuaqiu'] = (putData['feihuhuaqiu']) ? (1) : (0);
                putData['tianhulishang'] = (putData['tianhulishang']) ? (1) : (0);
                putData['xianhucaijue'] = (putData['xianhucaijue']) ? (1) : (0);
                putData['canghaisangtian'] = (putData['canghaisangtian']) ? (1) : (0);
                putData['yeyujiangnan'] = (putData['yeyujiangnan']) ? (1) : (0);
                putData['taichu'] = (putData['taichu']) ? (1) : (0);
                putData['shilifushou'] = (putData['shilifushou']) ? (1) : (0);
                putData['xiyangyang'] = (putData['xiyangyang']) ? (1) : (0);
                putData['mawangye'] = (putData['mawangye']) ? (1) : (0);
                putData['wanshengtianzun'] = (putData['wanshengtianzun']) ? (1) : (0);
                putData['yehuo'] = (putData['yehuo']) ? (1) : (0);
                putData['dulanggui'] = (putData['dulanggui']) ? (1) : (0);
                putData['vip9'] = (putData['vip9']) ? (1) : (0);
                putData['bihai'] = (putData['bihai']) ? (1) : (0);
                putData['changkong'] = (putData['changkong']) ? (1) : (0);
                putData['changong'] = (putData['changong']) ? (1) : (0);
                return putData;
            },
            search: function (e, type) {
                if (e) {
                    e.stopPropagation();
                }
                if (type == 'click') {
                    $scope.valueData.pageNo = 1;
                    $scope.resList = [];
                }
                if (!$scope.resList) {
                    $scope.resList = [];
                }
                $scope.isSearch = true;
                mainService.getData(fn.getFilterData()).then(function (res) {
                    if (res.status == 200) {
                        var result = res.data;
                        if (result.success) {
                            $scope.hasNext = result.result.hasNextPage;
                            if (result.result.hasNextPage) {
                                $scope.valueData.pageNo++;
                            }
                            $scope.resList = $scope.resList.concat(result.result.list);
                            $scope.total = result.result.total;
                        }
                    }
                });
            },
            searchPc: function (e, type) {
                if (e) {
                    e.stopPropagation();
                }
                if (type == 'click') {
                    $scope.valueData.pageNo = 1;
                    $scope.resList = [];
                }
                $scope.isSearch = true;
                mainService.getData(fn.getFilterData()).then(function (res) {
                    if (res.status == 200) {
                        var result = res.data;
                        if (result.success) {
                            $scope.resList = result.result.list;
                            $scope.total = result.result.total;
                            $scope.totalItem = Math.ceil(result.result.total / $scope.valueData.pageSize);
                        }
                    }
                });

            },
            changePage: function () {
                fn.searchPc();
            },
            checkPage: function (e, num) {
                if (!num || num < 0) {
                    $scope.valueData.pageNo = 1;
                }
                else if (num > $scope.totalItem) {
                    $scope.valueData.pageNo = $scope.totalItem;
                }
                else {
                    $scope.valueData.pageNo = num;
                }
                fn.searchPc();
            },
            reset: function (e) {
                e.stopPropagation();
                $scope.valueData = {
                    pageNo: $scope.valueData.pageNo,
                    pageSize: 10
                };
            }

        };

    };

    homeCtr.$inject = ['$rootScope', '$scope', 'mainService'];

    angular.module('home.controller', [])
        .controller('homeCtr', homeCtr);

})();