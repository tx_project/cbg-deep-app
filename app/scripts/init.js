/**
 * Created by yangdeng on 2016/12/10.
 */
'use strict';
(function () {

    var init = function () {

    };

    init.$inject = [];

    angular.module('myApp')
        .run(init);

})();