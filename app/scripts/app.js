'use strict';
var myApp = angular.module('myApp', [
    'ui.router', // Route service
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngDragDrop',
    'mgcrea.ngStrap',
    'ui.bootstrap',

    'myApp.directives',
    'myApp.services', // Services
    'myApp.controllers' // controller
]);