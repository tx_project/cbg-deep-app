/**
 * Created by yangdeng on 2016/12/10.
 */
'use strict';
/**
 * @description 项目头部
 */

(function() {

    var headerCtrl = function($scope,$rootScope,$state) {

        var headerData = $scope.headerData = {};

        $scope.goHome = function(e){
            e.stopPropagation();
            $state.go('home');
        };

        var fn = $scope.fn = {
        };
    };

    var header = function() {
        return {
            restrict: 'A',
            templateUrl: 'partials/header.html',
            controller: 'headerCtrl'
        };
    };

    headerCtrl.$inject = ['$scope','$rootScope','$state'];
    header.$inject = [];

    angular.module('header.directive', [])
        .controller('headerCtrl', headerCtrl)
        .directive('header', header);
})();