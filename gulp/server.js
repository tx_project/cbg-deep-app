'use strict';

var gulp = require('gulp');

var browserSync = require('browser-sync');

var proxy = require('proxy-middleware');
var url = require('url');

var proxyOptions;
function environment(target) {
    console.log('[\x1b[34mPM\x1b[0m] Rewrite \'api\' -> \'' + target + '\'');
    proxyOptions = url.parse(target + '/api');
    proxyOptions.route = '/api';
}


/* This configuration allow you to configure browser sync to proxy your backend */
// var proxyTarget = 'http://dev.yunbiaodan.cc:80'; // The location of your backend

function browserSyncInit(baseDir, files, browser) {
    browser = browser === undefined ? 'default' : browser;

    browserSync.instance = browserSync.init(files, {
        startPath: '/index.html',
        host: '0.0.0.0',
        server: {
            baseDir: baseDir,
            middleware: [proxy(proxyOptions)]
        },
        browser: browser
    });

}

/**
 * 线上环境
 */
gulp.task('serve', ['watch'], function () {
    environment('http://114.215.89.210:7001/');
    browserSyncInit([
        'app',
        '.tmp'
    ], [
        'app/*.html',
        '.tmp/styles/**/*.css',
        'app/scripts/**/*.js',
        'app/partials/**/*.html',
        'app/images/**/*'
    ]);
});

/**
 * 本地环境
 */
gulp.task('serve:local', ['watch'], function () {
    environment('http://localhost:15555');
    browserSyncInit([
        'app',
        '.tmp'
    ], [
        'app/*.html',
        '.tmp/styles/**/*.css',
        'app/scripts/**/*.js',
        'app/partials/**/*.html',
        'app/images/**/*'
    ]);
});

gulp.task('serve:dev', ['watch'], function () {
    environment('http://dev.yunbiaodan.cc:80');
    browserSyncInit([
        'app',
        '.tmp'
    ], [
        'app/*.html',
        '.tmp/styles/**/*.css',
        'app/scripts/**/*.js',
        'app/partials/**/*.html',
        'app/images/**/*'
    ]);
});

gulp.task('serve:test', ['watch'], function () {
    environment('http://test.yunbiaodan.cc:80');
    browserSyncInit([
        'app',
        '.tmp'
    ], [
        'app/*.html',
        '.tmp/styles/**/*.css',
        'app/scripts/**/*.js',
        'app/partials/**/*.html',
        'app/images/**/*'
    ]);
});
gulp.task('serve:grey', ['watch'], function () {
    environment('http://grey.yunbiaodan.cc:80');
    browserSyncInit([
        'app',
        '.tmp'
    ], [
        'app/*.html',
        '.tmp/styles/**/*.css',
        'app/scripts/**/*.js',
        'app/partials/**/*.html',
        'app/images/**/*'
    ]);
});
gulp.task('serve:dist', ['build'], function () {
    browserSyncInit('dist');
});

gulp.task('serve:e2e', function () {
    browserSyncInit(['app', '.tmp'], null, []);
});

gulp.task('serve:e2e-dist', ['watch'], function () {
    browserSyncInit('dist', null, []);
});
